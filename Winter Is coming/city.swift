//
//  city.swift
//  Winter Is coming
//
//  Created by Richard Smith on 2017-11-23.
//  Copyright © 2017 Richard Smith. All rights reserved.
//

import Foundation

struct Cities: Codable {
    let cities: [City]
}

struct City: Codable {
    let id: Int
    let name: String
    let temp: Float
}
